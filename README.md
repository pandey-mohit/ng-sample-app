# Angular Sample Application


# What is it?

  Sample Application is a Single Page Application(SPA) created on top of Angular JS.

  To run this example, follow this walkthrough:

  1. Install webstorm (https://www.jetbrains.com/webstorm/), a javscript IDE.

  2. Open project on webstorm.

  3. Go to index.html. Open index.html file.

  4. Hover on index.html file. You will see browser icon on top right corner. Click chrome icon.

  5. Hurray, sample application is now up and running on chrome browser.


# Contribution

  => pandey.mohit@live.com