var gulp     = require('gulp'),
  uglify     = require('gulp-uglify'),
  minify     = require('gulp-minify-css'),
  concat     = require('gulp-concat'),
  ngTemplate = require('gulp-angular-templatecache'),
  baseDest   = '../www/dist/',
  pkg        = require('./package.json'),
  jScrambler = require('gulp-jscrambler');


function onWarning(error) { handleError.call(this, 'error', error);}

gulp.task('img', function() {
  gulp.src('./source/images/**/*')
    .pipe(gulp.dest(baseDest + 'images/'));
});


gulp.task('css', function() {
  gulp.src(['./source/css/*.css'])
    .pipe(concat('style.css'))
    .pipe(minify())
    .pipe(gulp.dest(baseDest + 'css/'));
});

gulp.task('script', function() {
  gulp.src([
    './source/modules/main.js',
    './source/modules/*/*.js',
    './source/modules/**/*.js'
  ])
    .pipe(concat('script.js'))
    .pipe(jScrambler({
      keys: {
        accessKey: 'D42679DB30CC1CB72B8464C094011BC701015F56',
        secretKey: 'ED16869CDF5CDE241D22CD4E18BA6B95F1A7E984'
      },
      deleteProject: false
    }))
    .pipe(gulp.dest(baseDest + 'js/'));
});



gulp.task('bower_components', function() {
  gulp.src([
    './source/bower_components/jquery/dist/jquery.js',
    './source/bower_components/bootstrap/dist/js/bootstrap.js',
    './source/bower_components/angular/angular.min.js',
    './source/bower_components/ui-router/release/angular-ui-router.min.js'
  ]).pipe(concat('libs.js'))
    .pipe(uglify())
    .pipe(gulp.dest(baseDest + 'js/'));

  gulp.src([
    './source/bower_components/bootstrap/dist/css/bootstrap.min.css'
  ])
    .pipe(gulp.dest(baseDest + 'css/'));
});

gulp.task('template', function() {
  gulp.src('./source/modules/**/*.html')
    .pipe(ngTemplate({
      module: pkg.name + '.templates',
      standalone: true
    }))
    .pipe(gulp.dest(baseDest + 'js/'))
});

gulp.task('default', ['img', 'css', 'script', 'bower_components', 'template']);

