/*
 * Main Modules
 * Contains angular module dependencies
 * */

(function() {
  'use strict';

  angular.module('sampleApp', [
    'sampleApp.config',
    'sampleApp.app',
    'sampleApp.login',
    'sampleApp.home'
  ]);

})();

