(function() {
    'use strict';

    angular.module("sampleApp.login").
        config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state('login', {
                    url: '/',
                    templateUrl: 'modules/login/templates/login.html',
                    controller: 'LoginController'
                })
        }]);

})();