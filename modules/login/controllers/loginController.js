(function () {
  'use strict';

  angular.module('sampleApp.login')
    .controller('LoginController', ['$scope', '$rootScope', '$state', 'AppService',
      function ($scope, $rootScope, $state, AppService) {

        // navigate to home page
        $scope.login = function(){
          if(AppService.login()){
            $state.go('home');
          } else {
            alert('Error Occurred..');
          }
        };

      }])

})();
