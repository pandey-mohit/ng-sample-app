(function () {
    'use strict';

    angular.module('sampleApp.app')

        .config(['$httpProvider', function ($httpProvider) {
            // adding interceptor in httpProvider
            $httpProvider.interceptors.push("HttpInterceptor");
        }])

        .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', 'CONFIG',
            function ($stateProvider, $urlRouterProvider, $locationProvider, CONFIG) {
                // application start point
                $stateProvider
                    .state('app', {
                        url: '',
                        abstract: true,
                        templateUrl: 'modules/app/templates/base.html',
                        controller: 'AppController'
                    });

                $urlRouterProvider.otherwise(CONFIG.defaultRoute);
            }])

        // run method runs immediately after the application start
        .run(['$rootScope', '$location', '$timeout', 'AuthService', 'CONFIG',
            function ($rootScope, $location, $timeout, AuthService, CONFIG) {
                $rootScope.$on('$stateChangeStart', function () {
                    if(AuthService.session()) {
                        if($location.path() == '/'){
                            $location.path('/home');
                        }
                    } else {
                        $location.path('/');
                    }
                });
            }])

})();
