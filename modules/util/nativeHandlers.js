/*
* Native Utilities
* Contains native bridge method to communicate with mobile device
* */

(function() {
    'use strict';

    window.native = {
        onResume: function() {},
        onBackButton: function() {},
        alert: function(message, callback, title, buttonName) {
            if(message) {
                if(window.cordova) {
                    callback = callback || function() {};
                    title = title || 'Alert';
                    buttonName = buttonName || 'OK';
                    navigator.notification.alert(message, callback, title, buttonName)
                } else {
                    alert(message);
                }
            }
        }
    }

})();