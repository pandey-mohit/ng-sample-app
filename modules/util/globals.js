/*
* Global Utilities
* Contains generic methods
* */

 (function() {
    'use strict';

    window.globals = {
        getNumber: function(num) {
            return new Array(num || 0);
        },
        isMobile: function () {
            return /(iphone|ipod|ipad|android|blackberry|windows ce|palm|symbian)/i.test(navigator.userAgent);
        }
    }

})();