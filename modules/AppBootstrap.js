/*
 * Application Bootstrap
 * angular application starting point. App will start on DOM is ready
 * */


(function() {
    'use strict';

    function loadApp() {
        try {
            angular.bootstrap(document, ['sampleApp']);
        } catch (err) {
            console.log('error: ' + err);
        }
        if (window.cordova) {
            StatusBar.hide();
        }
    }
    // Listen to device ready
    angular.element(document).ready(function() {
        if (window.cordova) {
            document.addEventListener('deviceready', loadApp, false);
            document.addEventListener("resume", function(e) {
                native.onResume(e);
            }, false);
            document.addEventListener("backbutton", function(e) {
                native.onBackButton(e);
            }, false);
        } else {
            loadApp();
        }
    });

})();