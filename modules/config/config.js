/*
 * Configuration file
 * Contains app global configurations
 * */

(function() {
  'use strict';

  angular.module('sampleApp.config', [])
    .constant("CONFIG", {
      "appName": "sampleApp",
      "baseAPI": "https://example.com/",
      "defaultRoute": "/home"
    });

})();
